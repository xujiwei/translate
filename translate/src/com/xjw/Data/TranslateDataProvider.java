package com.xjw.Data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**      
 * 项目名称：translate  
 * 实现功能：  
 * 类名称：TranslateDataProvider   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014-10-12 下午1:27:49   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class TranslateDataProvider extends ContentProvider{

	private DBHelper dbHelper;
	private SQLiteDatabase database;
	private static final int DATA = 1; //操作单条记录
	private static final int DATAS = 2; //操作多条记录
	private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
	//定义匹配规则，data/#表示单挑记录， #代表数据的id
	static{
		URI_MATCHER.addURI("com.xjw.Data.TranslateDataProvider", "data", DATAS);
		URI_MATCHER.addURI("com.xjw.Data.TranslateDataProvider", "data/#", DATA);
	}
	@Override
	public boolean onCreate() {
		dbHelper = new DBHelper(getContext());
		return false;
	}

	//返回媒体类型，vnd.android.cursor.item/data表示单条记录 
	//vnd.android.cursor.dir/datas 表示多条记录
	@Override
	public String getType(Uri uri) {
		String type = "";
		switch (URI_MATCHER.match(uri)) {
			case DATA:
				type = "vnd.android.cursor.item/data";
				break;
			case DATAS:
				type = "vnd.android.cursor.dir/datas";
				break;
			default:
				break;
		}
		return type;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		database = dbHelper.getWritableDatabase();
		long recordId = database.insert("translate_data", "", values);
		Uri dataUrl = null;
		if (recordId > 0) {
			dataUrl = ContentUris.withAppendedId(uri, recordId);
		}
		return dataUrl;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		database = dbHelper.getWritableDatabase();
		int recordCount = database.delete("translate_data", selection, selectionArgs);
		return recordCount;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		database = dbHelper.getWritableDatabase();
		int recordCount = database.update("translate_data", values, selection, selectionArgs);
		return recordCount;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor cursor = null;
		database = dbHelper.getReadableDatabase();
		switch (URI_MATCHER.match(uri)) {
		case DATA:
			//如果是单条记录，则从url中解析id
			long id = ContentUris.parseId(uri);
			cursor = database.query("translate_data", projection, " _id = ?", new String[]{id+""}, null, null, sortOrder);
			break;
		case DATAS:
			cursor = database.query("translate_data", projection, selection, selectionArgs, null, null, sortOrder);
			break;

		default:
			break;
		}
		return cursor;
	}

}
