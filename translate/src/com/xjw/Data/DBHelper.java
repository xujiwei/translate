package com.xjw.Data;

import android.app.PendingIntent;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private static String name = "translate.db";
	private static int version = 2;
	private SQLiteDatabase database = null;

	public DBHelper(Context context) {
		super(context, name, null, version);
	}
    
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE translate_data (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, source_content TEXT(4000), result_content TEXT(4000), is_favorite INTEGER DEFAULT 0, create_time TEXT(100), is_shared INTEGER DEFAULT 0, shared_platform INTEGER DEFAULT 0, update_time TEXT(100), last_used_date TEXT(100))";
		db.execSQL(sql);
		sql = "CREATE TABLE reamind_data (_id  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,remind_type  INTEGER DEFAULT 0,remind_datetime  TEXT(100),remind_text  TEXT(1000),ringball  TEXT(200),is_reminded  INTEGER,is_vibration  INTEGER DEFAULT 1)";
		db.execSQL(sql);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "";
		for (int i = oldVersion; i <= newVersion; i++) {
			switch (i) {
			case 2:
				sql = "CREATE TABLE reamind_data (_id  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,remind_type  INTEGER DEFAULT 0,remind_datetime  TEXT(100),remind_text  TEXT(1000),ringball  TEXT(200),is_reminded  INTEGER,is_vibration  INTEGER DEFAULT 1)";
				db.execSQL(sql);
				break;

			default:
				break;
			}
		}
	}

	public SQLiteDatabase getDatabase() {
		if (database == null) {
			database = getReadableDatabase();
		}
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}

	

}
