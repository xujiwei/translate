package com.xjw.Data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LanguageAdapter {
	
	public static List<Map<String, Object>> getLanguageBaiDu(){
		
		List<Map<String, Object>> maps = new LinkedList<Map<String,Object>>();
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("code", "auto");
		map1.put("value", "�Զ�");
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("code", "zh");
		map2.put("value", "��");
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("code", "en");
		map3.put("value", "Ӣ");
		Map<String, Object> map4 = new HashMap<String, Object>();
		map4.put("code", "jp");
		map4.put("value", "��");
		Map<String, Object> map5 = new HashMap<String, Object>();
		map5.put("code", "kor");
		map5.put("value", "��");
		Map<String, Object> map6 = new HashMap<String, Object>();
		map6.put("code", "spa");
		map6.put("value", "��");
		Map<String, Object> map7 = new HashMap<String, Object>();
		map7.put("code", "fra");
		map7.put("value", "��");
		Map<String, Object> map8 = new HashMap<String, Object>();
		map8.put("code", "th");
		map8.put("value", "̩");
		Map<String, Object> map9 = new HashMap<String, Object>();
		map9.put("code", "ara");
		map9.put("value", "��");
		Map<String, Object> map10 = new HashMap<String, Object>();
		map10.put("code", "yue");
		map10.put("value", "��");
		Map<String, Object> map11 = new HashMap<String, Object>();
		map11.put("code", "ru");
		map11.put("value", "��");
		Map<String, Object> map12 = new HashMap<String, Object>();
		map12.put("code", "pt");
		map12.put("value", "��");
		maps.add(map1);
		maps.add(map2);
		maps.add(map3);
		maps.add(map4);
		maps.add(map5);
		maps.add(map6);
		maps.add(map7);
		maps.add(map8);
		maps.add(map9);
		maps.add(map10);
		maps.add(map11);
		maps.add(map12);
		return maps;
	}
	
	
	public static List<Map<String, Object>> getLanguageWindows(){
		
		List<Map<String, Object>> maps = new LinkedList<Map<String,Object>>();
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("code", "zh-CHS");
		map1.put("value", "��������");
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("code", "zh-CHT");
		map2.put("value", "��������");
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("code", "en");
		map3.put("value", "Ӣ��");
		Map<String, Object> map4 = new HashMap<String, Object>();
		map4.put("code", "ja");
		map4.put("value", "����");
		Map<String, Object> map5 = new HashMap<String, Object>();
		map5.put("code", "ko");
		map5.put("value", "����");
		Map<String, Object> map6 = new HashMap<String, Object>();
		map6.put("code", "fr");
		map6.put("value", "����");
		Map<String, Object> map7 = new HashMap<String, Object>();
		map7.put("code", "de");
		map7.put("value", "����");
		Map<String, Object> map8 = new HashMap<String, Object>();
		map8.put("code", "vi");
		map8.put("value", "Խ����");
		Map<String, Object> map9 = new HashMap<String, Object>();
		map9.put("code", "sv");
		map9.put("value", "�����");
		Map<String, Object> map10 = new HashMap<String, Object>();
		map10.put("code", "th");
		map10.put("value", "̩��");
		Map<String, Object> map11 = new HashMap<String, Object>();
		map11.put("code", "no");
		map11.put("value", "Ų����");
		Map<String, Object> map12 = new HashMap<String, Object>();
		map12.put("code", "es");
		map12.put("value", "��������");
		Map<String, Object> map13 = new HashMap<String, Object>();
		map13.put("code", "ru");
		map13.put("value", "����");
		Map<String, Object> map14 = new HashMap<String, Object>();
		map14.put("code", "pt");
		map14.put("value", "��������");
		Map<String, Object> map15 = new HashMap<String, Object>();
		map15.put("code", "ut");
		map15.put("value", "�������");
		Map<String, Object> map16 = new HashMap<String, Object>();
		map16.put("code", "tr");
		map16.put("value", "��������");
		Map<String, Object> map17 = new HashMap<String, Object>();
		map17.put("code", "ar");
		map17.put("value", "��������");
		maps.add(map1);
		maps.add(map2);
		maps.add(map3);
		maps.add(map4);
		maps.add(map5);
		maps.add(map6);
		maps.add(map7);
		maps.add(map8);
		maps.add(map9);
		maps.add(map10);
		maps.add(map11);
		maps.add(map12);
		return maps;
	}

}
