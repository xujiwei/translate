package com.xjw.entity;

import java.io.Serializable;

/**      
 * 项目名称：translate  
 * 实现功能：  记录对象
 * 类名称：TranslateData   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014-10-12 下午9:42:34   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class TranslateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String sourceContent;
	private String resultContent;
	private int isFavorite;
	private String createTime;
	private int isShared;
	private int sharedPlatform;
	private String updateTime;
	private String lastUsedDate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSourceContent() {
		return sourceContent;
	}
	public void setSourceContent(String sourceContent) {
		this.sourceContent = sourceContent;
	}
	public String getResultContent() {
		return resultContent;
	}
	public void setResultContent(String resultContent) {
		this.resultContent = resultContent;
	}
	public int getIsFavorite() {
		return isFavorite;
	}
	public void setIsFavorite(int isFavorite) {
		this.isFavorite = isFavorite;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getIsShared() {
		return isShared;
	}
	public void setIsShared(int isShared) {
		this.isShared = isShared;
	}
	public int getSharedPlatform() {
		return sharedPlatform;
	}
	public void setSharedPlatform(int sharedPlatform) {
		this.sharedPlatform = sharedPlatform;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getLastUsedDate() {
		return lastUsedDate;
	}
	public void setLastUsedDate(String lastUsedDate) {
		this.lastUsedDate = lastUsedDate;
	}
	
	
}
