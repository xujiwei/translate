package com.xjw.util;

public class CommonParam {
	
	public static final String BAIDU_URL = "http://openapi.baidu.com/public/2.0/bmt/translate";
	
	public static final String BAIDU_KEY = "ZL1VLTXwPGvXynqa74GaOLBj";
	
	public static final String BAIDU_CLIENTPARAM = "client_id";
	
	public static final String BAIDU_FROMPARAM = "from";
	
	public static final String BAIDU_TOPARAM = "to";
	
	public static final String BAIDU_CONTENTPARAM = "q";
	
	public static final String WINDOWS_URL = "http://api.microsofttranslator.com/v2/Http.svc/Translate";
	
	public static final String WINDOWS_KEY = "503BBB1CCBEDC4C8C246419445C05ABE11A9CF12";
	
	public static final String WINDOWS_CLIENTPARAM = "appId";
	
	public static final String WINDOWS_FROMPARAM = "from";
	
	public static final String WINDOWS_TOPARAM = "to";
	
	public static final String WINDOWS_CONTENTPARAM = "text";
	
	public static String username;
	public static String password;
	public static String perId;
	
	public static final String WEBSERBICE_URL = "http://192.168.1.100:8080/xfireTest/services/HelloWorld";
	public static final String WEBSERBICE_NAMESPACE = "http://service.xjw.com/HelloWorld/";
	
	public static final String WEBSERVICE_ADD = "add";
	
	public static final String xml_header_el = "soap:Header";
	public static final String xml_authentication_el = "Authenticate";
	public static final String xml_systemID_el = "systemID";
	public static final String xml_userID_el = "userID";
	public static final String xml_password_el = "password";
	public static final String LOG_TAG = "com.xjw.translate";
	
	/**
	 * 数据提供者URI
	 */
	public static final String TRANSLATE_DATA_PROVIDER_URI = "com.xjw.Data.TranslateDataProvider";

	/**
	 * 是否是调试模式
	 */
	public static final boolean IS_DEBUG_MODEL = false;
	/**
	 * 是否保存历史记录
	 */
	public static boolean isSaveHistory = true;
	/**
	 * 应用的地址
	 */
	public static final String APP_URL = "http://zhushou.360.cn/detail/index/soft_id/1574181";
	/**
	 * 讯飞语音appid
	 */
	public static final String XF_APPID = "appid=544f94b5";
	
	/**
	 * 历史记录数量
	 */
	public static int historyCount = 0;
	
	/**
	 * 我的收藏数量
	 */
	public static int favriteCount = 0;
	
	/**
	 * 备忘录相关数据提供者
	 */
	public static final String REMIND_DATA_PROVIDER_URI = "com.xjw.Data.RemindDataProvider";
}
