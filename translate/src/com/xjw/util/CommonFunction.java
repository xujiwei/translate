package com.xjw.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class CommonFunction {
	
	private static Toast toast = null;
	
	
	//检查网络连通状态
	public static boolean checkInternerState(Context context){
		boolean state = false;
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if (connectivityManager != null) {
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if (networkInfo != null ) {
				if (networkInfo.isConnected() && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
					return true;
				}
			}
		}
		
		
		return state;
	}
	
	/**
		* @方法名称: log
		* @描述: logcat记录
		* @param   @param infor 
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 上午11:30:24
	 */
	public static void  logCat(String infor){
		android.util.Log.i(CommonParam.LOG_TAG, infor);
	}
	
	/**
		* @方法名称: makeToast
		* @描述: 提示信息
		* @param   @param context
		* @param   @param flag
		* @param   @param message 
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 上午11:32:27
	 */
	public static void makeToast(Context context, int flag, String message){
		if (toast == null) {
			toast = Toast.makeText(context, message, flag);
		}else{
			toast.setText(message);
		}
		toast.show();
		
	}
	
	/**
		* @方法名称: makeToastShort
		* @描述: 短提示
		* @param   @param context
		* @param   @param resourceId 
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年10月26日 下午5:32:11
	 */
	public static void makeToastShort(Context context, int resourceId){
		if (toast == null) {
			toast = Toast.makeText(context, resourceId, 0);
		}else{
			toast.setText(resourceId);
		}
		toast.show();
		
	}
	
	/**
		* @方法名称: getCurrentFormatTime
		* @描述: 得到格式化后的当前时间
		* @param   @param format
		* @param   @return 
		* @return String 
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午2:08:55
	 */
	public static String getCurrentFormatTime(String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format,Locale.CHINA);
		Date now = new Date();
		String formatTime = sdf.format(now);;
		return formatTime;
	}
	
	/**
		* @方法名称: debugToast
		* @描述: debug模式提示信息
		* @param   @param context
		* @param   @param flag
		* @param   @param message 
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年10月19日 下午5:57:29
	 */
	public static void debugToastShort(Context context, String message){
		if (CommonParam.IS_DEBUG_MODEL) {
			if (toast == null) {
				toast = Toast.makeText(context, message, 0);
			}else{
				toast.setText(message);
			}
			toast.show();
		}
	}
	
	/**
		* @方法名称: list2SqlString
		* @描述: 将字符串列表转换成sql中的字符串条件
		* @param   @param list
		* @param   @return 
		* @return String 
		* @throws 
		* @author 徐纪伟
		* 2014年10月19日 下午6:13:30
	 */
	public static String list2SqlString(List<String> list, boolean isString){
		StringBuilder strResult = new StringBuilder();
		for (String s : list) {
			strResult.append(s).append(",");
		}
		return StringUtils.removeEnd(strResult.toString(), ",");
	}
	
	/**
		* @方法名称: timestamp
		* @描述: 返回格式为yyyyMMddHHmmssSSS的时间戳
		* @param   @return 
		* @return String 
		* @throws 
		* @author 徐纪伟
		* 2014年10月26日 上午11:06:36
	 */
	public static String timestamp() {
		String result = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date date = new Date();
		result = sdf.format(date);
		return result;
	}
	

}
