	package com.xjw.view;

import com.xjw.translate.R;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
	/**      
 * 项目名称：translate  
 * 实现功能： 弹出菜单公共类
 * 类名称：PopMenu   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月27日 下午9:32:49   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class PopMenu extends PopupWindow {

	private View mainView;
	private LinearLayout layoutSpeak, layoutListener, layoutListenerSource;
	
	public PopMenu(Activity paramActivity, View.OnClickListener paramOnClickListener, int paramInt1, int paramInt2) {
		super(paramActivity);
		 //窗口布局
	    mainView = LayoutInflater.from(paramActivity).inflate(R.layout.popwin_speak, null);
	    
	    layoutSpeak = ((LinearLayout)mainView.findViewById(R.id.layout_speak));
	    
	    layoutListener = (LinearLayout)mainView.findViewById(R.id.layout_listener);
	    
	    layoutListenerSource = (LinearLayout)mainView.findViewById(R.id.layout_listener_source);
	    //设置每个子布局的事件监听器
	    if (paramOnClickListener != null){
	    	layoutSpeak.setOnClickListener(paramOnClickListener);
	    	layoutListener.setOnClickListener(paramOnClickListener);
	    	layoutListenerSource.setOnClickListener(paramOnClickListener);
	    }
	    setContentView(mainView);
	    //设置宽度
	    setWidth(paramInt1);
	    //设置高度
	    setHeight(paramInt2);
	    //设置显示隐藏动画
	    setAnimationStyle(R.style.AnimTools);
	    //设置背景透明
	    setBackgroundDrawable(new ColorDrawable(0));
	}
}
