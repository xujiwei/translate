	package com.xjw.view;

import com.xjw.translate.R;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
	/**      
 * 项目名称：translate  
 * 实现功能：  选择语言下拉列表弹出窗口
 * 类名称：PopWinSpinner   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月18日 下午4:37:25   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class PopWinSpinner extends PopupWindow{
	public ListView ls_type;
	private View mainView;

  public PopWinSpinner(Activity paramActivity, AdapterView.OnItemClickListener paramOnItemClickListener, BaseAdapter paramBaseAdapter, int paramInt1, int paramInt2){
		 super(paramActivity);
	    this.mainView = LayoutInflater.from(paramActivity).inflate(R.layout.popwin_search, null);
	    this.ls_type = ((ListView)this.mainView.findViewById(R.id.listview_spinner));
	    this.ls_type.setAdapter(paramBaseAdapter);
	    if (paramOnItemClickListener != null){
	    	this.ls_type.setOnItemClickListener(paramOnItemClickListener);
	    }
	    setContentView(this.mainView);
	    setWidth(paramInt1);
	    setHeight(paramInt2);
	    setAnimationStyle(R.style.AnimAlpha);
	    setBackgroundDrawable(new ColorDrawable(0));
  	}
}
