package com.xjw.translate;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
	/**      
 * 项目名称：translate  
 * 实现功能：  
 * 类名称：ConfirmDialog   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月19日 下午7:28:39   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class ConfirmDialog extends Dialog implements android.view.View.OnClickListener{
	
	private Button btnOk,btnCancel;
	private TextView tvTitle,tvMessgae;
	private String title, message;
	private OnOkButtonClickListener listener;
	public ConfirmDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public ConfirmDialog(Context context, String title, String message, OnOkButtonClickListener listener) {
		//设置对提示框的样式，比如去除边框背景，添加自定义图片等
		super(context,R.style.ConfirmDialog);
		this.title = title;
		this.message = message;
		this.listener = listener;
		
	}
	
	@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			//设置提示框的样式布局
			setContentView(R.layout.confirm_dialog);
			btnOk = (Button)findViewById(R.id.btn_ok);
			btnCancel = (Button)findViewById(R.id.btn_cancel);
			btnOk.setOnClickListener(this);
			btnCancel.setOnClickListener(this);
			tvTitle = (TextView)findViewById(R.id.tv_confirm_title);
			tvMessgae = (TextView)findViewById(R.id.tv_confirm_messgae);
			tvTitle.setText(title);
			tvMessgae.setText(message);
		}

	/**
	 * 定义确定按钮和取消按钮的事件
	 */
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_ok:
			//点击确定按钮时，触发点击回调事件
			listener.OnOkButtonClick();
			break;
		case R.id.btn_cancel:
			dismiss();
			break;

		default:
			break;
		}
	}
	
	/**
		* @类名称: OnOkButtonClickListener
		* @描述: 使用接口回调的方式监听确定按钮点击事件
		* @throws 
		* @author 徐纪伟
		* 2014年10月24日下午10:12:29
	 */
	public interface OnOkButtonClickListener{
		public void OnOkButtonClick();
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OnOkButtonClickListener getListener() {
		return listener;
	}

	public void setListener(OnOkButtonClickListener listener) {
		this.listener = listener;
	}
	
	
		

}
