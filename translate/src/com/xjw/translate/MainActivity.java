package com.xjw.translate;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.iflytek.speech.RecognizerResult;
import com.iflytek.speech.SpeechError;
import com.iflytek.speech.SynthesizerPlayer;
import com.iflytek.ui.RecognizerDialog;
import com.iflytek.ui.RecognizerDialogListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;
import com.xjw.util.DisplayUtil;
import com.xjw.view.PopMenu;

public class MainActivity extends SlidingFragmentActivity {

	// 定义FragmentTabHost对象
	private FragmentTabHost mTabHost;

	// 定义一个布局
	private LayoutInflater layoutInflater;

	// 定义数组来存放Fragment界面
	private Class fragmentArray[] = { BaiduTranslate.class,
			WindowTranslate.class };

	// 定义数组来存放按钮图片
	private int mImageViewArray[] = { R.drawable.baidu, R.drawable.window };

	// Tab选项卡的文字
	private String mTextviewArray[] = { "百度翻译", "微软翻译" };

	// viewpager
	private ViewPager viewPager;
	// fragment数组
	private Fragment[] fragments;
	// 标题单选组
	private RadioGroup radioGroup;
	// 标题数组
	private String[] titles;
	// 标题宽度
	private int titleWidth = 0;
	// 滑动图片左边距
	private int splitImageLeft = 0;
	// 滑动图片
	private ImageView splitImage;
	// 标题图片资源数组
	private int[] titlebg;
	
	//收藏复选框
	private CheckBox cbFavorite;
	
	//左边菜单
	private SlidingMenu sm;
	
	//设置按钮
	private Button btnSetting;
	
	private ImageButton btnMain;
	
	private Context context;
	
	private ConfirmDialog confirmDialog;
	
	private PopMenu popMenu;
	
	private StringBuilder sbSpeak = new StringBuilder();

	/*
	 * 给item设置文字和图像
	 */
	private View getItemView(int position) {
		View view = layoutInflater.inflate(R.layout.tab_item_view, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
		imageView.setImageResource(mImageViewArray[position]);

		TextView textView = (TextView) view.findViewById(R.id.textview);
		textView.setText(mTextviewArray[position]);
		return view;
	}

	/*
	 * 初始化
	 */
	private void initView() {
		// 得到布局对象
		layoutInflater = LayoutInflater.from(this);
		// 得到tabhost对象
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		// mTabHost.setup(this,
		// getSupportFragmentManager(),R.id.realtabcontent);

		// 分页的个数
		int count = fragmentArray.length;

		// 循环设置view
		for (int i = 0; i < count; i++) {
			// 新建一个分页，设置显示的文字 以及图像
			TabSpec tabSpec = mTabHost.newTabSpec(mTextviewArray[i])
					.setIndicator(getItemView(i));
			// 将新建的分页加入到tabhost中
			mTabHost.addTab(tabSpec, fragmentArray[i], null);
			// 设置选中的分页的背景颜色
			mTabHost.getTabWidget().getChildAt(i)
					.setBackgroundResource(R.drawable.selector_tab_background);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// 版本2代码
		// initView();
		// 版本3代码
		initSlidingMenu();
		initFragment();
		setListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * 视图初始化
	 */
	private void initFragment() {
		context = this;
		layoutInflater = LayoutInflater.from(this);
		viewPager = (ViewPager) findViewById(R.id.mViewPager);
		Fragment fragmentBaidu = new BaiduTranslate();
		Fragment fragmentWindow = new WindowTranslate();
		fragments = new Fragment[] { fragmentBaidu };

		MyViewPagerAdapter adapter = new MyViewPagerAdapter(
				getSupportFragmentManager());
		viewPager.setAdapter(adapter);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
		titles = getResources().getStringArray(R.array.titles);
		titlebg = new int[] { R.drawable.baidu, R.drawable.window };
		// 计算每个标题的宽度
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics dm = new DisplayMetrics();
		display.getMetrics(dm);
		titleWidth = dm.widthPixels / titles.length;
		splitImage = (ImageView) findViewById(R.id.splitImage);
		LayoutParams layoutParams = splitImage.getLayoutParams();
		layoutParams.width = titleWidth;
		// 动态添加radiobutton
		RadioButton rb = null;
		radioGroup.removeAllViews();
		LayoutInflater inflater = LayoutInflater.from(this);
		for (int i = 0; i < titles.length; i++) {
			// 从布局中读取单选按钮布局
			rb = (RadioButton) inflater.inflate(R.layout.nav_radiogroup_item,
					null);
			// 设置显示文字
			rb.setText(titles[i]);
			// 设置id。不能丢，区分用
			rb.setId(i);
			rb.setButtonDrawable(titlebg[i]);

			rb.setLayoutParams(new LayoutParams(titleWidth,
					LayoutParams.MATCH_PARENT));
			if (i == 0) {
				rb.setChecked(true);
			}
			radioGroup.addView(rb);
		}
		
		cbFavorite = (CheckBox)findViewById(R.id.cb_favorite);
		btnSetting = (Button)findViewById(R.id.btn_setting);
		btnMain = (ImageButton)findViewById(R.id.btn_main);
	}

	/**
	 * @方法名称: setListener
	 * @描述: 设置监听器
	 * @param
	 * @return void
	 * @throws
	 * @author 徐纪伟 2014-10-12 上午11:21:58
	 */
	private void setListener() {
		// 单选按钮事件
		radioGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						// 选中时viewpager联动
						viewPager.setCurrentItem(checkedId);
						View radioButton = group.getChildAt(checkedId);
						// 创建位移动画,从当前位置移动到新位置
						Animation animation = new TranslateAnimation(
								splitImageLeft, radioButton.getLeft(), 0f, 0f);
						animation.setDuration(200);
						animation.setFillAfter(true);
						splitImage.startAnimation(animation);
						// 记录当前位置
						splitImageLeft = radioButton.getLeft();
					}
				});
		// viewpager选中事件
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int index) {
				// 选中时模拟单选按钮的单击事件
				((RadioButton) radioGroup.getChildAt(index)).performClick();

			}

			@Override
			public void onPageScrolled(int index, float value, int pix) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		
		//收藏复选框事件
		cbFavorite.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
//					final Dialog dialog = new Dialog(MainActivity.this, R.style.ConfirmDialog);
//					dialog.setContentView(R.layout.confirm_dialog);
//					Button button = (Button)dialog.findViewById(R.id.btn_cancel);
//					button.setOnClickListener(new View.OnClickListener() {
//						@Override
//						public void onClick(View v) {
//							dialog.dismiss();
//						}
//					});
//					dialog.show();
					 final AlertDialog dlg = new AlertDialog.Builder(MainActivity.this).create();
					 dlg.show();
					 dlg.setContentView(R.layout.confirm_dialog);
					       
					 Button cancel = (Button) dlg.findViewById(R.id.btn_cancel);
					 cancel.setOnClickListener(new View.OnClickListener() {
						  public void onClick(View v) {
							  dlg.dismiss();
						  }
					 });
					 Button ok = (Button) dlg.findViewById(R.id.btn_ok);
					 ok.setOnClickListener(new View.OnClickListener() {
						 public void onClick(View v) {
							CommonFunction.debugToastShort(context, "确定按钮");
						 }
					 });
					 
				}
			}
		});
		
		//设置按钮事件
		btnSetting.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sm.toggle();
				
			}
		});
		
		btnMain.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (popMenu == null) {
					OnMenuClickListener onMenuClickListener = new OnMenuClickListener();
					popMenu = new PopMenu(MainActivity.this, onMenuClickListener, DisplayUtil.dip2px(context, 160), DisplayUtil.dip2px(context, 160));
					popMenu.getContentView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
						
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								popMenu.dismiss();
							}
						}
					});
				}
				popMenu.setFocusable(true);;
				//以某个控件的x和y的偏移量位置开始显示窗口
				popMenu.showAsDropDown(btnMain, 0, 0);
				//如果窗口存在，则更新
				popMenu.update();
				
			}
		});
	}

	/**
	 * 自定义fragment适配器
	 * 
	 * @author xujiwei
	 * 
	 */
	class MyViewPagerAdapter extends FragmentPagerAdapter {

		public MyViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int index) {
			return fragments[index];
		}

		@Override
		public int getCount() {
			return fragments.length;
		}

	}
	
	private void initSlidingMenu(){
		// 设置滑动菜单的视图
		setBehindContentView(R.layout.fragment_left);
		// 实例化滑动菜单对象
		sm = getSlidingMenu();
		//将该视图使用自定义的fragment来替换，要显示的视图在fragment中处理
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_left, new FragmentLeft()).commit();		
		//menu.addIgnoredView(viewPager);
		//左右两边菜单
		sm.setMode(SlidingMenu.LEFT);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		// 设置滑动阴影的宽度
		sm.setShadowWidthRes(R.dimen.shadow_width);
		// 设置滑动阴影的图像资源
		sm.setShadowDrawable(R.drawable.left_shadow);
		// 设置滑动菜单视图的宽度
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		// 设置渐入渐出效果的值
		sm.setFadeDegree(0.35f);
		//getSupportFragmentManager().beginTransaction().replace(R.id.fragmrnt_left, new FragmentLeft()).commit();
	}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			confirmDialog = new ConfirmDialog(context, "提示信息", "确认要退出应用吗？", new ConfirmDialog.OnOkButtonClickListener() {
				@Override
				public void OnOkButtonClick() {
					exitApp();
				}
			});
			confirmDialog.show();
			break;
		case KeyEvent.KEYCODE_VOLUME_UP:// 音量增大
			mAudioManager.adjustStreamVolume (AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI); 
			break;
		case KeyEvent.KEYCODE_VOLUME_DOWN:// 音量减小
			mAudioManager.adjustStreamVolume (AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI); 
			break;
		default:
			break;
		}
		return true;
	}
	
	/**
		* @方法名称: exitApp
		* @描述: 退出应用
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年10月21日 下午9:28:36
	 */
	private void exitApp(){
		finish();
		System.exit(0);
	}
	
	/**
			* @类名称: OnMenuClickListener
			* @描述: 主界面菜单监听器类
			* @throws 
			* @author 徐纪伟
			* 2014年10月27日下午9:45:03
	 */
	class OnMenuClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.layout_speak:
				CommonFunction.debugToastShort(context, "语音录入");
				// 这是语言识别部分，最重要的实例化一个
				// RecognizerDialog并把你在官方网站申请的appid填入进去，非法id不能进行识别
				RecognizerDialog isrDialog = new RecognizerDialog(
						context, CommonParam.XF_APPID);

				/*
				 * 设置引擎目前支持五种 ”sms”：普通文本转写 “poi”：地名搜索 ”vsearch”：热词搜索
				 * ”video”：视频音乐搜索 ”asr”：命令词识别
				 */
				isrDialog.setEngine("sms", null, null);
				isrDialog.setListener(recoListener);
				isrDialog.show();
				popMenu.dismiss();
				break;
			case R.id.layout_listener_source:
				popMenu.dismiss();
				CommonFunction.debugToastShort(context, "播报原文");
				EditText editTextSource = (EditText)fragments[0].getActivity().findViewById(R.id.baiduSource);
				SynthesizerPlayer player1 = SynthesizerPlayer
						.createSynthesizerPlayer(context, CommonParam.XF_APPID);
				player1.playText(editTextSource.getText().toString(), "ent=vivi21,bft=5",null);
				break;
			case R.id.layout_listener:
				popMenu.dismiss();
				CommonFunction.debugToastShort(context, "播报结果");
				EditText editTextResult = (EditText)fragments[0].getActivity().findViewById(R.id.baiduResult);
				// 这是后台朗读，实例化一个SynthesizerPlayer
				SynthesizerPlayer player2 = SynthesizerPlayer
						.createSynthesizerPlayer(context, CommonParam.XF_APPID);
				// 设置语音朗读者，
				//player.setVoiceName("xiaoyan");
				player2.playText(editTextResult.getText().toString(), "ent=vivi21,bft=5",null);
				break;
			default:
				break;
			}
			
		}
		
	}
	
    //语音识别监听器
	private RecognizerDialogListener recoListener = new RecognizerDialogListener() {

		@Override
		public void onResults(ArrayList<RecognizerResult> results,
				boolean isLast) {
				// 服务器识别完成后会返回集合，
				sbSpeak = new StringBuilder();
				sbSpeak.append(results.get(0).text);
				CommonFunction.debugToastShort(context, sbSpeak.toString());
		}			

		@Override
		public void onEnd(SpeechError error) {
			if (error == null) {
				// 完成后就把结果显示在EditText上
				EditText editTextSource = (EditText)fragments[0].getActivity().findViewById(R.id.baiduSource);
				editTextSource.setText(sbSpeak.toString());
			}
		}

	};
	
	

}
