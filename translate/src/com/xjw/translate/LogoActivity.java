	package com.xjw.translate;

import com.xjw.util.CommonFunction;
import com.xjw.util.DisplayUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.Handler;
import android.text.BoringLayout.Metrics;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
	/**      
 * 项目名称：translate  
 * 实现功能：  显示Logo界面
 * 类名称：LogoActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月25日 上午11:18:08   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class LogoActivity extends Activity {
	private ImageView imgLogo;
	private Display dispaly;
	private DisplayMetrics displayMetrics;
	private Resources resource;
	private Context context;
	private TextView tvLogo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logo);
		initView();
	}
	
	private void initView() {
		context = LogoActivity.this;
		imgLogo = (ImageView)findViewById(R.id.img_logo);
		tvLogo = (TextView)findViewById(R.id.tv_logo);
		dispaly = getWindowManager().getDefaultDisplay();
		displayMetrics = new DisplayMetrics();
		dispaly.getMetrics(displayMetrics);
		resource = getResources();
		imgLogo.startAnimation(AnimationUtils.loadAnimation(context,R.anim.anim_logo_show));
//		TranslateAnimation animation = new TranslateAnimation(0, 0-DisplayUtil.dip2px(context, 160)-45, 0, 0);
//		animation.setDuration(500);
//		animation.setFillAfter(true);
//		animation.setInterpolator(new OvershootInterpolator());
//		imgLogo.startAnimation(animation);
		//tvLogo.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_scale_logo));
		
		AnimationSet anim2 = new AnimationSet(true);
		ScaleAnimation myscaleanim = new ScaleAnimation(1f,3f,1f,3f,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
		myscaleanim.setDuration(1000);
		
		AlphaAnimation myalphaanim = new AlphaAnimation(1,0.0001f);
		myalphaanim.setDuration(1500);
		anim2.addAnimation(myscaleanim);
		anim2.addAnimation(myalphaanim);
		anim2.setStartOffset(1000);
		anim2.setFillAfter(true);
		tvLogo.startAnimation(anim2);
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				Intent intent = new Intent(LogoActivity.this,MainActivity.class);
				startActivity(intent);
				finish();
			}
		}, 1500);
		CommonFunction.debugToastShort(context, "启动完成！");
	}
}
