	package com.xjw.translate;

import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;
	/**      
 * 项目名称：translate  
 * 实现功能：  系统设置
 * 类名称：MoreActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月23日 下午9:20:28   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class SettingActivity extends Activity {
	
	private RelativeLayout layoutHistory,layoutClearFavrite,layoutClearHistory, layoutShare,layoutMarket;
	private Context context;
	private CheckBox cbHistory;
	private SharedPreferences sp;
	private Editor editor;
	private String[] titles;
	private Intent intent;
	private ConfirmDialog confirmDialog;
	private Resources resource;
	private ContentResolver contentResolver;
	private ProgressDialog dialog;
	private Button btnBack;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		initView();
		setListener();
		
	}
	
	private void initView() {
		context = SettingActivity.this;
		layoutHistory = (RelativeLayout)findViewById(R.id.layout_history);
		layoutClearFavrite = (RelativeLayout)findViewById(R.id.layout_clear_favrite);
		layoutClearHistory = (RelativeLayout)findViewById(R.id.layout_clear_history);
		layoutShare = (RelativeLayout)findViewById(R.id.layout_share_app);
		layoutMarket = (RelativeLayout)findViewById(R.id.layout_market);
		cbHistory = (CheckBox)findViewById(R.id.cb_history);
		sp = getSharedPreferences("config",MODE_PRIVATE);
		editor = sp.edit();
		resource = getResources();
		titles = resource.getStringArray(R.array.menu_left_Item_name);
		intent = getIntent();
		setTitle(titles[intent.getIntExtra("index", 0)]);
		cbHistory.setChecked(CommonParam.isSaveHistory);
		contentResolver = getContentResolver();
		btnBack = (Button)findViewById(R.id.btn_back_setting);
	}
	
	private void setListener() {
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		layoutHistory.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean isSaveHistory = sp.getBoolean("isSaveHistory", true);
				editor.putBoolean("isSaveHistory", !isSaveHistory);
				editor.commit();
				cbHistory.setChecked(!isSaveHistory);
				CommonParam.isSaveHistory = !isSaveHistory;
			}
		});
		
		layoutClearFavrite.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				confirmDialog = new ConfirmDialog(context, resource.getString(R.string.confirm_title_normal), resource.getString(R.string.clear_favrite), new ConfirmDialog.OnOkButtonClickListener() {
					
					@Override
					public void OnOkButtonClick() {
						confirmDialog.dismiss();
						dialog = new ProgressDialog(context);
						dialog.setTitle("提示");
						dialog.setMessage("正在加载，请稍后...");
						dialog.show();
						Uri uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
						contentResolver.delete(uri, " is_favorite = ?", new String[]{"1"});
						dialog.dismiss();
						CommonFunction.makeToast(context, 0, resource.getString(R.string.clear_favrite_success));
						
					}
				});
				confirmDialog.show();
			}
		});
		
		layoutClearHistory.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				confirmDialog = new ConfirmDialog(context, resource.getString(R.string.confirm_title_normal), resource.getString(R.string.clear_history), new ConfirmDialog.OnOkButtonClickListener() {
					
					@Override
					public void OnOkButtonClick() {
						confirmDialog.dismiss();
						dialog = new ProgressDialog(context);
						dialog.setTitle("提示");
						dialog.setMessage("正在加载，请稍后...");
						dialog.show();
						Uri uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
						contentResolver.delete(uri, " is_favorite = ?", new String[]{"0"});
						dialog.dismiss();
						CommonFunction.makeToast(context, 0, resource.getString(R.string.clear_history_success));
					}
				});
				confirmDialog.show();
			}
		});
		
		layoutShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CommonFunction.debugToastShort(context, "应用分享");
				Intent localIntent2 = new Intent();
				localIntent2.setAction(Intent.ACTION_SEND);
				localIntent2.putExtra(Intent.EXTRA_SUBJECT, "应用分享");
				StringBuilder sb = new StringBuilder();
				sb.append(CommonParam.APP_URL).append("\n 应用名称：全能翻译");
				localIntent2.putExtra(Intent.EXTRA_TEXT, sb.toString());
				localIntent2.setType("text/plain");
				startActivity(Intent.createChooser(localIntent2, "应用分享"));
				
			}
		});
		
		layoutMarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 try{
		            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + context.getPackageName())));
		         }catch (ActivityNotFoundException localActivityNotFoundException){
		        	CommonFunction.makeToast(context, 0, "找不到应用市场！");
		         }
				
			}
		});
	}
}
