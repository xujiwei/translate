package com.xjw.translate;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.xjw.Data.LanguageAdapter;
import com.xjw.util.CommonParam;

public class WindowTranslate extends Fragment implements OnTouchListener{
	
	private Spinner spinnerFrom,spinnerTo;
	private Button translateButton,convertButton;
	private EditText editTextFrom,editTextTo;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.window_translate, null);
	}
	
	@Override
	public void onStart() {
		
		spinnerFrom = (Spinner)getView().findViewById(R.id.windowFrom);
		spinnerTo = (Spinner)getView().findViewById(R.id.windowTo);
		translateButton = (Button)getView().findViewById(R.id.windowTranslate);
		convertButton = (Button)getView().findViewById(R.id.windowConvert);
		editTextFrom = (EditText)getView().findViewById(R.id.windowSource);
		editTextTo = (EditText)getView().findViewById(R.id.windowResult);
		editTextFrom.setBackgroundColor(Color.WHITE);
		editTextTo.setBackgroundColor(Color.WHITE);
		
		//SimpleAdapter 使用负责数据对象，List<Map<String, String>>
		List<Map<String, Object>> maps = LanguageAdapter.getLanguageWindows();
		SimpleAdapter adapter = new SimpleAdapter(getView().getContext(), maps, android.R.layout.simple_dropdown_item_1line, new String[]{"value"}, new int[]{android.R.id.text1});
		
		spinnerFrom.setAdapter(adapter);
		spinnerFrom.setSelection(2);
		spinnerTo.setAdapter(adapter);
		spinnerTo.setSelection(0);
		translateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editTextFrom.getWindowToken(), 0);
				editTextTo.setText("");
				Map<String, Object> mapFrom = (Map<String, Object>)spinnerFrom.getSelectedItem();
				Map<String, Object> mapTo = (Map<String, Object>)spinnerTo.getSelectedItem();
				String codeFrom = mapFrom.get("code").toString();
				String codeTo = mapTo.get("code").toString();
				String source = editTextFrom.getText().toString().trim();
				if (source.equals("")) {
					Toast.makeText(getView().getContext(), R.string.notNull, Toast.LENGTH_SHORT).show();
					return;
				}
				//同种翻译时的验证，不翻译
				if (codeFrom.equals(codeTo) && !codeFrom.equals("auto")) {
					 Toast.makeText(getView().getContext(), R.string.sameCode, Toast.LENGTH_SHORT).show();
				}else{
					StringBuilder sb = new StringBuilder();
					try {
						sb.append(CommonParam.WINDOWS_URL)
						  .append("?").append(CommonParam.WINDOWS_CLIENTPARAM).append("=").append(CommonParam.WINDOWS_KEY)
						  .append("&").append(CommonParam.WINDOWS_FROMPARAM).append("=").append(codeFrom)
						  .append("&").append(CommonParam.WINDOWS_TOPARAM).append("=").append(codeTo)
						  .append("&").append(CommonParam.WINDOWS_CONTENTPARAM).append("=").append(URLEncoder.encode(source, "utf-8"));
					} catch (UnsupportedEncodingException e) {
						Toast.makeText(getView().getContext(), R.string.translateError, Toast.LENGTH_SHORT).show();
					}
					
				    new TranslateTask().execute(sb.toString());
							
				}
			}
		});
		
		convertButton.setOnTouchListener(this);
		
		convertButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = spinnerFrom.getSelectedItemPosition();
				spinnerFrom.setSelection(spinnerTo.getSelectedItemPosition());
				spinnerTo.setSelection(temp);
			}
		});
		
		super.onStart();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (MotionEvent.ACTION_DOWN == event.getAction()) {
			v.setBackgroundResource(R.drawable.convertfocus);
		}else if(MotionEvent.ACTION_UP == event.getAction()){
			v.setBackgroundResource(R.drawable.convertnormal);
		}
		return false;
	}
	
	class TranslateTask extends AsyncTask<String, Integer, String>{

		@Override
		protected String doInBackground(String... params) {
			StringBuilder result = new StringBuilder();
			try {
				URL url = new URL(params[0]);
				InputStream inputStream = url.openStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
				String receive = "";
				if ( (receive = bufferedReader.readLine()) != null) {
					result.append(receive);
				}
			} catch (Exception e) {
				return "-1";
			}
			return result.toString();
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result.equals("-1")) {
				Toast.makeText(getView().getContext(), R.string.translateError, Toast.LENGTH_SHORT).show();
				return ;
			}
			String resultStr = result.toString();
			resultStr = resultStr.substring(resultStr.indexOf(">")+1, resultStr.indexOf("</"));
			editTextTo.setText(resultStr);
			super.onPostExecute(result);
		}
		
		
	}
	
}
