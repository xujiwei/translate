package com.xjw.translate;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import com.xjw.entity.TranslateData;
import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;
import com.xjw.util.DisplayUtil;
import com.xjw.view.PopWinShare;

/**      
 * 项目名称：translate  
 * 实现功能：  翻译记录详情
 * 类名称：TranslateDataContentActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014-10-13 下午9:29:34   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class TranslateDataContentActivity extends Activity {

	private TextView tvSource;
	private TextView tvResult;
	private TextView tvCreateTime;
	private TranslateData translateData;
	private Button btnBack, btnCopy;
	private CheckBox cbIsFavorite ;
	private ContentResolver contentResolver;
	private Context context;
	private ImageButton btnTools;
	private PopWinShare popWinShare;
	private List<String> listShare;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.translate_content_activity);
		initView();
		setListener();
	}
	
	private void initView(){
		context = this;
		contentResolver = getContentResolver();
		tvSource = (TextView)findViewById(R.id.tv_content_source);
		tvResult = (TextView)findViewById(R.id.tv_content_result);
		btnBack = (Button)findViewById(R.id.btn_content_back);
		btnCopy = (Button)findViewById(R.id.btn_copy);
		tvCreateTime = (TextView)findViewById(R.id.tv_createtime);
		cbIsFavorite = (CheckBox)findViewById(R.id.cb_listview_favorite);
		btnTools = (ImageButton)findViewById(R.id.btn_tools);
		Intent intent = getIntent();
		translateData = (TranslateData)intent.getSerializableExtra("data");
		tvSource.setText(translateData.getSourceContent());
		tvResult.setText(translateData.getResultContent());
		tvCreateTime.setText(translateData.getUpdateTime());
		cbIsFavorite.setChecked(translateData.getIsFavorite() == 1);
	}
	
	private void setListener(){
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		cbIsFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		  
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				String where = " _id = ? ";
				String selectionArgs[] = new String[]{translateData.getId()+""};
				Uri uri = null;
				ContentValues values = new ContentValues();
				String strTips = "";
				if (isChecked) {
					values.put("is_favorite", 1);
					strTips = "添加收藏成功！";
				}else{
					values.put("is_favorite", 0);
					strTips = "取消收藏成功！";
				}
				uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
				int count = contentResolver.update(uri, values, where, selectionArgs);
				if (count > 0) {
					CommonFunction.makeToast(context, 0, strTips);
				}
			}
		});
		
		btnCopy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ClipboardManager cmb = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);  
				ClipData clipData = ClipData.newPlainText("result", tvResult.getText());
				cmb.setPrimaryClip(clipData);
				CommonFunction.makeToast(context, 0, "翻译结果已经复制到剪切板！");
			}
		});
		
		btnTools.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (popWinShare == null) {
					//自定义的单击事件
					OnClickLintener paramOnClickListener = new OnClickLintener();
					popWinShare = new PopWinShare(TranslateDataContentActivity.this, paramOnClickListener, DisplayUtil.dip2px(context, 160), DisplayUtil.dip2px(context, 160));
					//监听窗口的焦点事件，点击窗口外面则取消显示
					popWinShare.getContentView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
						
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								popWinShare.dismiss();
							}
						}
					});
				}
				//设置默认获取焦点
				popWinShare.setFocusable(true);
				//以某个控件的x和y的偏移量位置开始显示窗口
				popWinShare.showAsDropDown(btnTools, 0, 0);
				//如果窗口存在，则更新
				popWinShare.update();
			}
		});
	}
	
	/**
			* @类名称: OnClickLintener
			* @描述: 工具栏popwindow点击事件
			* @throws 
			* @author 徐纪伟
			* 2014年10月25日下午2:21:44
	 */
	class OnClickLintener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.layout_share:
				listShare = new ArrayList<String>();
				CommonFunction.debugToastShort(context, "分享内容");
				Intent localIntent2 = new Intent();
//				ComponentName comp = new ComponentName("com.tencent.mm",
//                        "com.tencent.mm.ui.tools.ShareToTimeLineUI");
//				localIntent2.setComponent(comp);
				localIntent2.setAction(Intent.ACTION_SEND);
				localIntent2.putExtra(Intent.EXTRA_SUBJECT, "分享内容");
				StringBuilder sb = new StringBuilder();
				sb.append("原文:\n").append("  ").append(tvSource.getText()).append("\n结果：\n").append("  ").append(tvResult.getText()).append("\n 消息来源：全能翻译");
				listShare.add("原文:");
				listShare.add("  " + tvSource.getText().toString());
				listShare.add("结果:");
				listShare.add("  " + tvResult.getText().toString());
				listShare.add("消息来源：全能翻译");
				localIntent2.putExtra(Intent.EXTRA_TEXT, sb.toString());
				//Uri imageUri =  Uri.parse("android.resource://com.xjw.translate/"+R.raw.s20141026101202);
				//String imagePath = Environment.getExternalStorageDirectory() + Environment.DIRECTORY_PICTURES + CommonFunction.timestamp()+".jpg";
				
//				File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//				File imageFile = new File(dir, CommonFunction.timestamp()+".jpg");
//				text2Image(listShare, imageFile);
//				Uri imageUri = Uri.fromFile(imageFile);
//				localIntent2.putExtra(Intent.EXTRA_STREAM, imageUri);
//				localIntent2.setType("image/*");
				localIntent2.setType("text/plain");
				
				startActivity(Intent.createChooser(localIntent2, "内容分享"));
				//startActivity(localIntent2);
				break;
			case R.id.layout_copy:
				CommonFunction.debugToastShort(context, "复制结果");
				ClipboardManager cmb = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);  
				ClipData clipData = ClipData.newPlainText("result", tvResult.getText());
				cmb.setPrimaryClip(clipData);
				CommonFunction.makeToast(context, 0, "翻译结果已经复制到剪切板！");
				break;
                  
			default:
				break;
			}
			
		}
		
	}
	
	public static boolean text2Image(List<String> listShare, File imageFile){
		Bitmap bitmap = Bitmap.createBitmap(200, 40 + listShare.size() * 20,Config.ARGB_8888);//创建一个你需要尺寸的Bitmap
		Canvas canvas = new Canvas(bitmap);//用这个Bitmap生成一个Canvas,然后canvas就会把内容绘制到上面这个bitmap中
		canvas.drawColor(Color.WHITE);//绘制背景颜色
		Paint paint = new Paint();//定义一个画笔
		paint.setColor(Color.BLACK);//画笔颜色
		paint.setTextSize(14);
		
		String text = "";
		for (int i = 0 ; i < listShare.size(); i++) {
			text = listShare.get(i);
			canvas.drawText(text, 20, i * 20 + 20 , paint);//绘制文案
		}
		
		//绘制Logo
//		Bitmap logo = xxx;//获得你的Logobitmap
//		canvas.drawBitmap(logo, x,y, logo.getWidth(), logo.getHeight());
		canvas.save();
		try {
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(imageFile));
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
