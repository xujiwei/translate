	package com.xjw.translate;

import com.xjw.util.CommonParam;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
/**      
 * 项目名称：translate  
 * 实现功能：  自定义application
 * 类名称：TranslateApplication   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月24日 下午9:35:14   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class TranslateApplication extends Application {

	private SharedPreferences sp;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		sp = getSharedPreferences("config", MODE_PRIVATE);
		CommonParam.isSaveHistory = sp.getBoolean("isSaveHistory", true);
	}
}
