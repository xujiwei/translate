package com.xjw.translate;

import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentLeft extends Fragment {
	
	private static Context context;
	private View view;
	private String[] itemNameArray;
	private int[] itemImageArray;
	private static MenuLeftListViewAdapter adapter;
	private ListView listView;
	private Class activityAttay[] = { HistoryActivity.class,HistoryActivity.class,SettingActivity.class,AboutActivity.class,MoreActivity.class};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.menu_left, container,false);
		return rootView;
	}
	
	@Override
	public void onStart() {
		CommonFunction.debugToastShort(context, "onStart");
		context = getActivity();
		view = getView();
		itemNameArray = getResources().getStringArray(R.array.menu_left_Item_name);
		itemImageArray = new int[]{R.drawable.menu_left_history,R.drawable.menu_left_favorite,R.drawable.menu_left_set,R.drawable.menu_left_more_icon,R.drawable.menu_left_about_icon};
		adapter = new MenuLeftListViewAdapter(context);
		listView = (ListView)view.findViewById(R.id.menu_left_listview);
		listView.setAdapter(adapter);
		setListtener();
		super.onStart();
	}
	
	/**
		* @方法名称: setListtener
		* @描述: 设置监听器
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午9:10:40
	 */
	private void setListtener(){
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(context, activityAttay[position]);
				intent.putExtra("index", position);
				startActivity(intent);
			}
		});
	}
	
	/**
		* @类名称: MenuLeftListViewAdapter
		* @描述: 菜单项数据适配器
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午9:09:52
	 */
	public class MenuLeftListViewAdapter extends BaseAdapter{
		
		private Context context;
		
		public MenuLeftListViewAdapter(Context context){
			this.context = context;
		}

		@Override
		public int getCount() {
			
			return itemNameArray.length;
		}

		@Override
		public Object getItem(int position) {
			
			return itemNameArray[position];
		}

		@Override
		public long getItemId(int position) {
			
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null ) {
				convertView = LayoutInflater.from(context).inflate(R.layout.menu_left_listview_item, null);
			}
			ImageView imageView = (ImageView)convertView.findViewById(R.id.item_image);
			TextView textView = (TextView)convertView.findViewById(R.id.item_name);
			TextView tvNumber = (TextView)convertView.findViewById(R.id.item_num);
			imageView.setBackgroundResource(itemImageArray[position]);
			textView.setText(itemNameArray[position]);
			//只有历史记录和我的收藏显示数量
			if (position == 0 || position == 1) {
				tvNumber.setVisibility(View.VISIBLE);
				//历史记录数量
				if (position == 0) {
					tvNumber.setText(CommonParam.historyCount+"");
				}
				//我的收藏数量
				if (position == 1) {
					tvNumber.setText(CommonParam.favriteCount+"");
				}
			}
			return convertView;
		}
		
	}
	
	@Override
	public void onAttach(Activity activity) {
		CommonFunction.debugToastShort(context, "onAttach");
		super.onAttach(activity);
	}
	
	@Override
	public void onResume() {
		CommonFunction.debugToastShort(context, "onResume");
		new Thread(new ThreadCount()).start();
		super.onResume();
	}
	
	private static Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				adapter.notifyDataSetChanged();
				break;

			default:
				break;
			}
			super.handleMessage(msg);
		}
	};
	
	/**
			* @类名称: ThreadCount
			* @描述: 获取历史记录以及收藏记录数量线程类
			* @throws 
			* @author 徐纪伟
			* 2014年10月29日下午9:02:41
	 */
	public static class ThreadCount implements Runnable{

		@Override
		public void run() {			
			ContentResolver contentResolver = context.getContentResolver();
			Uri uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
			Cursor cursor = contentResolver.query(uri, new String[]{"is_favorite","count(_id) as num"}, " 1=1 GROUP BY is_favorite", null, " is_favorite desc");
			int iIsFavrite = -1;
			while (cursor.moveToNext()) {
				//根据收藏标记来确定数量
				iIsFavrite = cursor.getInt(cursor.getColumnIndex("is_favorite"));
				if (iIsFavrite == 1) {
					CommonParam.favriteCount = cursor.getInt(cursor.getColumnIndex("num"));
				}else if (iIsFavrite == 0) {
					CommonParam.historyCount = cursor.getInt(cursor.getColumnIndex("num"));
				}
			}
			//发送消息到handler去处理
			Message message = Message.obtain();
			message.what = 1;
			handler.sendMessage(message);
		}
		
	}
	
}
