package com.xjw.translate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import android.app.Activity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xjw.entity.TranslateData;
import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;

/**      
 * 项目名称：translate  
 * 实现功能：  
 * 类名称：HistoryActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014-10-12 下午9:09:25   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class HistoryActivity extends Activity {
	
	private ListView historyListView;
	private TranslateListViewAdapter adapter;
	private LoaderManager manager;
	private LoaderManager.LoaderCallbacks<Cursor> callbacks;
	private List<TranslateData> datas;
	private Button btnBack,btnChexiao,btnDelete;
	private ContentResolver contentResolver;
	private Context context;
	private boolean isMutSelected;
	private Map<Integer, Boolean> cbStatus;
	private List<String> selectIds;
	private RelativeLayout layoutButtom;
	private TextView tvSelectedCount,tvTitle;
	private ProgressDialog dialog;
	private CheckBox cbAllSelect;
	private ConfirmDialog confirmDialog;
	private boolean isCheckClick = false;
	private int index = 0;
	private String[] titles;
	private int count = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history_activity);
		initView();
		
	}
	
	/**
		* @方法名称: initView
		* @描述: 初始化视图
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午9:39:32
	 */
	private void initView(){
		context = this;
		titles = getResources().getStringArray(R.array.menu_left_Item_name);
		historyListView = (ListView)findViewById(R.id.history_listview);
		btnBack = (Button)findViewById(R.id.btn_history_back);
		adapter = new TranslateListViewAdapter(this);
		contentResolver = getContentResolver();
		isMutSelected = false;
		cbStatus = new HashMap<Integer, Boolean>();
		selectIds = new ArrayList<String>();
		layoutButtom = (RelativeLayout)findViewById(R.id.history_bottom_layout);
		btnChexiao = (Button)findViewById(R.id.btn_chexiao);
		btnDelete = (Button)findViewById(R.id.btn_delete);
		tvSelectedCount = (TextView)findViewById(R.id.tv_selected);
		cbAllSelect = (CheckBox)findViewById(R.id.cb_all_select);
		tvTitle = (TextView)findViewById(R.id.tv_record_title);
		
		dialog = new ProgressDialog(context);
		dialog.setTitle("提示");
		dialog.setMessage("正在加载，请稍后...");
		setListener();
		manager = getLoaderManager();
		dialog.show();
		Intent intent = getIntent();
		index = intent.getIntExtra("index", 0);
		tvTitle.setText(titles[index]);
		manager.initLoader(1001, null, callbacks);
		
	}
	
	private void setListener(){
		callbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
			
			@Override
			public void onLoaderReset(Loader<Cursor> loader) {
				
			}
			
			@Override
			public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
				datas = new ArrayList<TranslateData>();
				TranslateData translateData = null;
				while (data.moveToNext()) {
					translateData = new TranslateData();
					translateData.setId(data.getInt(data.getColumnIndex("_id")));
					translateData.setSourceContent(data.getString(data.getColumnIndex("source_content")));
					translateData.setResultContent(data.getString(data.getColumnIndex("result_content")));
					translateData.setIsFavorite(data.getInt(data.getColumnIndex("is_favorite")));
					translateData.setCreateTime(data.getString(data.getColumnIndex("create_time")));
					translateData.setIsShared(data.getInt(data.getColumnIndex("is_shared")));
					translateData.setSharedPlatform(data.getInt(data.getColumnIndex("shared_platform")));
					translateData.setUpdateTime(data.getString(data.getColumnIndex("update_time")));
					translateData.setLastUsedDate(data.getString(data.getColumnIndex("last_used_date")));
					datas.add(translateData);
				}
				adapter.setData(datas);
				historyListView.setAdapter(adapter);
				adapter.notifyDataSetChanged();
				dialog.dismiss();
				if (datas.size() == 0) {
					CommonFunction.makeToast(context, 0, "暂时还没有记录！");
				}
			}
			
			@Override
			public Loader<Cursor> onCreateLoader(int id, Bundle args) {
				CursorLoader loader = new CursorLoader(HistoryActivity.this);
				Uri url = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
				loader.setUri(url);
				loader.setSelection(" is_favorite = "+index+" ");
				loader.setSortOrder("update_time desc");
				//还可以设置映射的列，条件，参数，排序，分组等
				return loader;
			}
			
		};
		
		historyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TranslateData translateData = null;
				ListView listView = (ListView)parent;
				translateData = (TranslateData)listView.getItemAtPosition(position);
				if (!isMutSelected) {
					Intent intent = new Intent(HistoryActivity.this, TranslateDataContentActivity.class);
					intent.putExtra("data", translateData);
					startActivity(intent);
				}else{
					CheckBox chkMutSelect = (CheckBox)view.findViewById(R.id.cb_mut_select);
					if (chkMutSelect.isChecked()) {
						chkMutSelect.setChecked(false);
						selectIds.remove(translateData.getId()+"");
					}else{
						chkMutSelect.setChecked(true);
						selectIds.add(translateData.getId()+"");
					}
					cbStatus.put(position, chkMutSelect.isChecked());
					tvSelectedCount.setText(selectIds.size()+"项已选中");
				}
				
			}
		});
		
		historyListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (isMutSelected) {
					return false;
				}

				cbStatus.clear();
				for (int i = 0; i < datas.size(); i++) {
					cbStatus.put(i, false);
				}
				isMutSelected = true;
				adapter.notifyDataSetChanged();
				tvSelectedCount.setText(selectIds.size()+"项已选中");
				animShow(layoutButtom);
				//animShowButtomLayout();
				animShow(cbAllSelect);
				layoutButtom.setVisibility(View.VISIBLE);
				cbAllSelect.setVisibility(View.VISIBLE);
				return true;
			}
			
		});		
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		btnChexiao.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				exitMutSelected();
			}
		});
		
		btnDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				if (selectIds.size() == 0) {
					CommonFunction.makeToast(context, 0, "请选中所要删除的条目！");
					return;
				}else{
					 confirmDialog = new ConfirmDialog(context, "系统提示", "确认要删除所选记录吗？ ", new ConfirmDialog.OnOkButtonClickListener() {
						@Override
						public void OnOkButtonClick() {
							confirmDialog.dismiss();
							Uri url = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
							int count = contentResolver.delete(url, " _id in ("+CommonFunction.list2SqlString(selectIds, false)+") ", null);
							if (count > 0) {
								CommonFunction.makeToast(context, 0, "删除成功！");
								dialog.show();
								for (int i = 0; i < cbStatus.size(); i++) {
									cbStatus.put(i, false);
								}
								selectIds.clear();
								manager.restartLoader(1001, null, callbacks);
								tvSelectedCount.setText(selectIds.size()+"项已选中");
							}
							
						}
					});
					confirmDialog.show();
				}
			}
		});
		
		cbAllSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				isCheckClick = true;
				selectIds.clear();
				View eachView = null;
				CheckBox cbMutSelect = null;
				for (int i = 0; i < adapter.getCount(); i++) {
					eachView = adapter.getView(i, null, null);
					cbMutSelect = (CheckBox)eachView.findViewById(R.id.cb_mut_select);
					cbMutSelect.setChecked(isChecked);
					if (isChecked) {
						TranslateData translateData = null;
						translateData = (TranslateData)historyListView.getItemAtPosition(i);
						selectIds.add(translateData.getId()+"");
					}
					cbStatus.put(i, isChecked);
				}
				adapter.notifyDataSetChanged();
				isCheckClick = false;
				tvSelectedCount.setText(selectIds.size()+"项已选中");
			}
		});;
	}
	
	/**
		* @类名称: TranslateListViewAdapter
		* @描述: 记录数据适配器
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午9:47:23
	 */
	class TranslateListViewAdapter extends BaseAdapter{

		private Context context;
		private List<TranslateData> data;
		
		public TranslateListViewAdapter(Context context){
			this.context = context;
		}
		
		public void setData(List<TranslateData> data){
			this.data = data;
		}
		
		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int i = getCount();
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.translata_data_item, null);
			}
			TextView sourceTextView = (TextView)convertView.findViewById(R.id.source_text);
			TextView resultTextView = (TextView)convertView.findViewById(R.id.result_text);
			TextView createDateTextView = (TextView)convertView.findViewById(R.id.text_create_time);
			final CheckBox chkMutSelect = (CheckBox)convertView.findViewById(R.id.cb_mut_select);
			final TranslateData translateData = (TranslateData)getItem(position);
			if (isMutSelected) {
				
				if (!isCheckClick) {
					animShow(chkMutSelect);
					CommonFunction.debugToastShort(context, "1");
				}
				chkMutSelect.setVisibility(CheckBox.VISIBLE);
//				convertView.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						if (chkMutSelect.isChecked()) {
//							chkMutSelect.setChecked(false);
//							selectIds.remove(translateData.getId()+"");
//						}else{
//							chkMutSelect.setChecked(true);
//							selectIds.add(translateData.getId()+"");
//						}
//						cbStatus.put(finalPostion, chkMutSelect.isChecked());
//						tvSelectedCount.setText(selectIds.size()+"项已选中");
//					}
//				});
				chkMutSelect.setChecked(cbStatus.get(position));
			}else{
				animFade(chkMutSelect);
				chkMutSelect.setVisibility(View.GONE);
			}
			//CheckBox cbIsFavorite = (CheckBox)convertView.findViewById(R.id.cb_listview_favorite);
			
			String sourceContent = translateData.getSourceContent();
			if (sourceContent.length() > 8) {
				sourceContent = sourceContent.substring(0, 8);
			}
			String resultContent = translateData.getResultContent();
			if (resultContent.length() > 15) {
				resultContent = resultContent.substring(0, 15) + "...";
			}
			sourceTextView.setText(sourceContent);
			resultTextView.setText(resultContent);
			createDateTextView.setText(translateData.getUpdateTime());
//			cbIsFavorite.setChecked(translateData.getIsFavorite() == 1);
//			cbIsFavorite.setTag(translateData.getId());
//			//点击事件，
//			cbIsFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//				  
//				@Override
//				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//					String where = " _id = ? ";
//					String selectionArgs[] = new String[]{buttonView.getTag()+""};
//					Uri uri = null;
//					ContentValues values = new ContentValues();
//					String strTips = "";
//					if (isChecked) {
//						values.put("is_favorite", 1);
//						strTips = "添加收藏成功！";
//					}else{
//						values.put("is_favorite", 0);
//						strTips = "取消收藏成功！";
//					}
//					uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
//					int count = contentResolver.update(uri, values, where, selectionArgs);
//					if (count > 0) {
//						CommonFunction.makeToast(context, 0, strTips);
//					}
//				}
//			});
			return convertView;
		}
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		manager.restartLoader(1001, null, callbacks);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//多选时按返回键，取消多选状态
		if(keyCode == KeyEvent.KEYCODE_BACK && isMutSelected){
			exitMutSelected();
			return true;
		}else{
			return super.onKeyDown(keyCode, event);
		}
	}	
	
	/**
		* @方法名称: exitMutSelected
		* @描述: 退出多选
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年10月21日 下午9:13:36
	 */
	private void exitMutSelected(){
		isMutSelected = false;
		//animShowButtomLayout();
		animFade(cbAllSelect);
		animFade(layoutButtom);
		layoutButtom.setVisibility(View.INVISIBLE);
		cbAllSelect.setVisibility(View.INVISIBLE);
		cbAllSelect.setChecked(false);
		selectIds.clear();
		adapter.notifyDataSetChanged();
	}
	
	private void animFade(View view){
		view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_alpha_fade));
	}
	private void animShow(View view){
		view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_alpha_show));
	}
	
	private void animShowButtomLayout(){
		layoutButtom.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_trans_bottom_show));
	}
	private void animHideButtomLayout(){
		layoutButtom.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_trans_bottom_hide));
	}
	
}
