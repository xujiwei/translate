	package com.xjw.translate;

import java.util.List;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xjw.util.CommonFunction;
	/**      
 * 项目名称：translate  
 * 实现功能：  更多应用
 * 类名称：MoreActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月23日 下午9:20:28   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class AboutActivity extends Activity {
	
	private String[] titles;
	private Intent intent;
	private Resources resource;
	private Context context;
	private Button btnBack;
	private TextView tv;
	private RelativeLayout layoutQQ,layoutMail;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		initView();
		setListener();
	}
	
	private void initView() {
		context = AboutActivity.this;
		btnBack = (Button)findViewById(R.id.btn_back_about);
		resource = getResources();
		titles = resource.getStringArray(R.array.menu_left_Item_name);
		intent = getIntent();
		setTitle(titles[intent.getIntExtra("index", 0)]);
		tv = (TextView)findViewById(R.id.tv_mail);
		layoutQQ = (RelativeLayout)findViewById(R.id.layout_qq);
		layoutMail = (RelativeLayout)findViewById(R.id.layout_mail);

	}
	
	private void setListener() {
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		layoutMail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Uri uri = Uri.parse("mailto:" + resource.getString(R.string.mail_value));
				Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
				startActivity(intent);
//				Intent intent = new Intent(Intent.ACTION_SEND);  
//				intent.putExtra(Intent.EXTRA_EMAIL, resource.getString(R.string.mail_value));  
//				intent.putExtra(Intent.EXTRA_TEXT, "");  
//				intent.putExtra(Intent.EXTRA_SUBJECT, "使用建议(全能翻译)");    
//				intent.setType("message/rfc822");  
//				startActivity(Intent.createChooser(intent, "Choose Email Client"));
				
			}
		});
		
		layoutQQ.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try{
					ClipboardManager cmb = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);  
					ClipData clipData = ClipData.newPlainText("qq_value", resource.getString(R.string.qq_value));
					cmb.setPrimaryClip(clipData);
					CommonFunction.makeToast(context, 0, "qq号码已复制至剪切板！");
//					Intent intent = new Intent(Intent.ACTION_MAIN);
//					ComponentName componentName = new ComponentName(
//					"com.tencent.mobileqq",
//					"com.tencent.mobileqq.activity.SplashActivity");
//					intent.setComponent(componentName);
//					context.startActivity(intent);
					
				}catch(Exception e){
					CommonFunction.makeToast(context, 0, "未安装qq！");
				}
			}
		});

	}
}
