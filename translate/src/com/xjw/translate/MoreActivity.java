	package com.xjw.translate;

import com.xjw.util.CommonFunction;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
	/**      
 * 项目名称：translate  
 * 实现功能：  关于应用
 * 类名称：MoreActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月23日 下午9:20:28   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class MoreActivity extends Activity {

	private String[] titles;
	private Intent intent;
	private Resources resource;
	private Context context;
	private Button btnBack;
	private GridView gvMore;
	private String[] gridTitles = new String[]{"备忘录","翻译"};
	private int[] gridButtons = new int[]{R.drawable.selector_gridview_item1,R.drawable.selector_gridview_item1};
	private Class[] clazz = new Class[]{RemindActivity.class};
	private MyGridViewAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more);
		initView();
		setListener();
	}
	
	private void initView() {
		context = MoreActivity.this;
		btnBack = (Button)findViewById(R.id.btn_back_more);
		resource = getResources();
		titles = resource.getStringArray(R.array.menu_left_Item_name);
		intent = getIntent();
		setTitle(titles[intent.getIntExtra("index", 0)]);
		gvMore = (GridView)findViewById(R.id.gv_more);
		adapter = new MyGridViewAdapter(context);
		gvMore.setAdapter(adapter);

	}
	
	private void setListener() {
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	}
	
	/**
			* @类名称: MyGridViewAdapter
			* @描述: gridview自定义数据适配器
			* @throws 
			* @author 徐纪伟
			* 2014年10月31日下午9:51:54
	 */
	class MyGridViewAdapter extends BaseAdapter{
		private Context context;
		
		public MyGridViewAdapter(Context context) {
			this.context = context;
		}
		
		@Override
		public int getCount() {
			return gridTitles.length;
		}

		@Override
		public Object getItem(int position) {
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.more_griview_item, null);
			}
			ImageButton ibGridView = (ImageButton)convertView.findViewById(R.id.ib_gridview);
			ibGridView.setBackgroundResource(gridButtons[position]);
			final int index = position;
			//图片按钮单击事件
			ibGridView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					CommonFunction.debugToastShort(context, gridTitles[index]);
					Intent intent = new Intent(MoreActivity.this,clazz[index]);
					startActivity(intent);
				}
			});
			TextView tvGridView = (TextView)convertView.findViewById(R.id.tv_gridview);
			tvGridView.setText(gridTitles[position]);
			return convertView;
		}
	}
}
