package com.xjw.translate;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.xjw.Data.DBHelper;
import com.xjw.Data.LanguageAdapter;
import com.xjw.translate.R.layout;
import com.xjw.util.CommonFunction;
import com.xjw.util.CommonParam;
import com.xjw.util.DisplayUtil;
import com.xjw.view.PopWinSpinner;

public class BaiduTranslate extends Fragment implements OnTouchListener{
	
	private Spinner spinnerFrom,spinnerTo;
	private Button translateButton,convertButton;
	private EditText editTextFrom,editTextTo;
	private DBHelper dbHelper;
	private ContentResolver transDataProvider;
	private String source = "";
	private TextView tvFromText, tvFromCode, tvToText, tvToCode;
	private PopWinSpinner popWinFrom;
	private PopWinSpinner popWinTo;
	private LayoutInflater layoutInflater;
	private Context context;
	private List<Map<String, Object>> datas;
	private TypeAdapter typeAdapter;
	private RelativeLayout layoutFrom, layoutTo;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.baidu_translate, null);
	}
	
	@Override
	public void onStart() {
		context = getActivity();
		transDataProvider = getActivity().getContentResolver();
		spinnerFrom = (Spinner)getView().findViewById(R.id.baiduFrom);
		spinnerTo = (Spinner)getView().findViewById(R.id.baiduTo);
		translateButton = (Button)getView().findViewById(R.id.baiduTranslate);
		convertButton = (Button)getView().findViewById(R.id.baiduConvert);
		editTextFrom = (EditText)getView().findViewById(R.id.baiduSource);
		editTextTo = (EditText)getView().findViewById(R.id.baiduResult);
		//editTextFrom.setBackgroundColor(Color.WHITE);
		//editTextTo.setBackgroundColor(Color.WHITE);
		tvFromText = (TextView)getView().findViewById(R.id.tv_from_text);
		tvFromCode = (TextView)getView().findViewById(R.id.tv_from_code);
		tvToText = (TextView)getView().findViewById(R.id.tv_to_text);
		tvToCode = (TextView)getView().findViewById(R.id.tv_to_code);
		layoutFrom = (RelativeLayout)getView().findViewById(R.id.layout_from);
		layoutTo = (RelativeLayout)getView().findViewById(R.id.layout_to);
		layoutInflater = LayoutInflater.from(getActivity());
		datas = LanguageAdapter.getLanguageBaiDu();
		typeAdapter = new TypeAdapter(context);
		typeAdapter.setDatas(datas);
		//SimpleAdapter 使用负责数据对象，List<Map<String, String>>
		List<Map<String, Object>> maps = LanguageAdapter.getLanguageBaiDu();
		SimpleAdapter adapter = new SimpleAdapter(getView().getContext(), maps, R.layout.spinner_item_view, new String[]{"value"}, new int[]{R.id.tv_spinner_item});
		
		spinnerFrom.setAdapter(adapter);
		spinnerTo.setAdapter(adapter);
		
		translateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!CommonFunction.checkInternerState(context)) {
					CommonFunction.makeToastShort(context, R.string.no_internet);
					return;
				}
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editTextFrom.getWindowToken(), 0);
				editTextTo.setText("");
				Map<String, Object> mapFrom = (Map<String, Object>)spinnerFrom.getSelectedItem();
				Map<String, Object> mapTo = (Map<String, Object>)spinnerTo.getSelectedItem();
//				String codeFrom = mapFrom.get("code").toString();
//				String codeTo = mapTo.get("code").toString();
				String codeFrom = tvFromCode.getText().toString();
				String codeTo = tvToCode.getText().toString();
				source = editTextFrom.getText().toString().trim();
				if (source.equals("")) {
					//Toast.makeText(getView().getContext(), R.string.notNull, Toast.LENGTH_SHORT).show();
					CommonFunction.makeToastShort(context, R.string.notNull);
					return;
				}
				//同种翻译时的验证，不翻译
				if (codeFrom.equals(codeTo) && !codeFrom.equals("auto")) {
					CommonFunction.makeToastShort(context, R.string.sameCode);
					 //Toast.makeText(getView().getContext(), R.string.sameCode, Toast.LENGTH_SHORT).show();
				}else{
					StringBuilder sb = new StringBuilder();
					try {
						sb.append(CommonParam.BAIDU_URL)
						  .append("?").append(CommonParam.BAIDU_CLIENTPARAM).append("=").append(CommonParam.BAIDU_KEY)
						  .append("&").append(CommonParam.BAIDU_FROMPARAM).append("=").append(codeFrom)
						  .append("&").append(CommonParam.BAIDU_TOPARAM).append("=").append(codeTo)
						  .append("&").append(CommonParam.BAIDU_CONTENTPARAM).append("=").append(URLEncoder.encode(source, "utf-8"));
					} catch (UnsupportedEncodingException e) {
						//Toast.makeText(getView().getContext(), R.string.translateError, Toast.LENGTH_SHORT).show();
						CommonFunction.makeToastShort(context, R.string.translateError);
					}
					
					new TranslateTask().execute(sb.toString());
							
				}
			}
		});
		
		//convertButton.setOnTouchListener(this);
		
		convertButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = spinnerFrom.getSelectedItemPosition();
				spinnerFrom.setSelection(spinnerTo.getSelectedItemPosition());
				spinnerTo.setSelection(temp);
				String tempValue = tvFromText.getText().toString();
				String tempCode = tvFromCode.getText().toString();
				tvFromText.setText(tvToText.getText());
				tvFromCode.setText(tvToCode.getText());
				tvToText.setText(tempValue);
				tvToCode.setText(tempCode);
			}
		});
		
		layoutFrom.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (popWinFrom == null) {
					popWinFrom = new PopWinSpinner(getActivity(), new TypeOnItemClickListener(), typeAdapter, DisplayUtil.dip2px(context, 100), DisplayUtil.dip2px(context, 150));
				}
				popWinFrom.getContentView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
					
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (!hasFocus) {
							popWinFrom.dismiss();
						}
						
					}
				});
				popWinFrom.setFocusable(true);
				popWinFrom.showAsDropDown(layoutFrom, 0, -5);
				popWinFrom.update();
			}
		});
		
		layoutTo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (popWinTo == null) {
					popWinTo = new PopWinSpinner(getActivity(), new TypeToOnItemClickListener(), typeAdapter, DisplayUtil.dip2px(context, 100), DisplayUtil.dip2px(context, 150));
				}
				popWinTo.getContentView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
					
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (!hasFocus) {
							popWinTo.dismiss();
						}
						
					}
				});
				popWinTo.setFocusable(true);
				popWinTo.showAsDropDown(layoutTo, 0, -5);
				popWinTo.update();
			}
		});
		
		super.onStart();
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (MotionEvent.ACTION_DOWN == event.getAction()) {
			v.setBackgroundResource(R.drawable.convertfocus);
		}else if(MotionEvent.ACTION_UP == event.getAction()){
			v.setBackgroundResource(R.drawable.convertnormal);
		}
		return false;
	}
	
	/**
		* @类名称: TranslateTask
		* @描述: 调用接口处理返回数据异步任务类
		* @throws 
		* @author 徐纪伟
		* 2014-10-12 下午12:30:07
	 */
	class TranslateTask extends AsyncTask<String, Integer, String>{

		@Override
		protected String doInBackground(String... params) {
			
			StringBuilder result = new StringBuilder();
			try {
				URL url = new URL(params[0]);
				InputStream inputStream = url.openStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
				String receive = "";
				if ( (receive = bufferedReader.readLine()) != null) {
					result.append(receive);
				}
			} catch (Exception e) {
				return "-1";
			}
			return result.toString();
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result.equals("-1")) {
				CommonFunction.makeToastShort(context, R.string.notNull);
				//Toast.makeText(getView().getContext(), R.string.notNull, Toast.LENGTH_SHORT).show();
				return;
			}
			try {
				JSONObject jsonObject = new JSONObject(result.toString());
				 
				 if (jsonObject.opt("error_code") != null) {
					 CommonFunction.makeToastShort(context, R.string.notNull);
					 //Toast.makeText(getView().getContext(), R.string.notNull, Toast.LENGTH_SHORT).show();
					 return;
				 }
				 JSONArray array = (JSONArray)jsonObject.get("trans_result");
				 StringBuilder resultStr = new StringBuilder("");
				for (int i = 0; i < array.length(); i++) {
					JSONObject jsonObject2 = (JSONObject)array.get(i);
					System.out.println(jsonObject2.get("dst"));
					resultStr.append(jsonObject2.get("dst") + "");
				}
				//显示结果
				editTextTo.setText(resultStr.toString());
				
				//记录查询历史
				if (CommonParam.isSaveHistory) {
					//先检查是否查询过相同的内容，如果存在，则更新最新结果以及更新时间，如果不存在则直接插入新内容
					Uri uri = Uri.parse("content://"+CommonParam.TRANSLATE_DATA_PROVIDER_URI+"/data");
					ContentValues values = new ContentValues();
					Cursor cursor = transDataProvider.query(uri, null, " source_content = ? ", new String[]{source}, null);
					if (cursor.moveToNext()) {
						CommonFunction.debugToastShort(context, "存在相同的");
						values.put("result_content", resultStr.toString());
						String timeNow = CommonFunction.getCurrentFormatTime("yyyy-MM-dd HH:mm:ss");
						values.put("update_time", timeNow);
						transDataProvider.update(uri, values, " source_content = ? ", new String[]{source});
					}else{
						values.put("source_content", source);
						values.put("result_content", resultStr.toString());
						String timeNow = CommonFunction.getCurrentFormatTime("yyyy-MM-dd HH:mm:ss");
						values.put("create_time", timeNow);
						values.put("update_time", timeNow);
						values.put("last_used_date", timeNow);
						transDataProvider.insert(uri, values);
						new Thread(new FragmentLeft.ThreadCount()).start();
					}
					
					
					
				}
				
			} catch (Exception e) {
				CommonFunction.makeToastShort(context, R.string.translateError);;
				 //Toast.makeText(getView().getContext(), R.string.notNull, Toast.LENGTH_SHORT).show();
			}
			
			super.onPostExecute(result);
		}
		
	}
	
	/**
	* @类名称: TypeOnItemClickListener
	* @描述: 语言种类选择listView点击事件
	* @throws 
	* @author 徐纪伟
	* 2014年10月18日下午8:21:18
	*/
	class TypeOnItemClickListener implements AdapterView.OnItemClickListener{
	
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Map<String, Object> map = (Map<String, Object>)parent.getItemAtPosition(position);
			String value = map.get("value").toString();
			String code = map.get("code").toString();
			tvFromText.setText(value);
			tvFromCode.setText(code);
			popWinFrom.dismiss();
		}
		
	}
	
	class TypeToOnItemClickListener implements AdapterView.OnItemClickListener{
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Map<String, Object> map = (Map<String, Object>)parent.getItemAtPosition(position);
			String value = map.get("value").toString();
			String code = map.get("code").toString();
			tvToText.setText(value);
			tvToCode.setText(code);
			popWinTo.dismiss();
		}
		
	}

	/**
		* @类名称: TypeAdapter
		* @描述: 语言种类数据适配器
		* @throws 
		* @author 徐纪伟
		* 2014年10月18日下午9:50:51
	 */
	class TypeAdapter extends BaseAdapter{
	
		private Context context;
		private List<Map<String, Object>> datas;
		public TypeAdapter (Context context){
			this.context = context;
		}
		
		public void setDatas(List<Map<String, Object>> datas) {
			this.datas = datas;
		}
		@Override
		public int getCount() {
			return datas.size();
		}
	
		@Override
		public Object getItem(int position) {
			
			return datas.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return position;
		}
	
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.search_type_item, null);
			}
			TextView tvType = (TextView)convertView.findViewById(R.id.tv_item_search);
			tvType.setText(((Map<String, Object>)getItem(position)).get("value").toString());
			return convertView;
		}
		
	}

}
