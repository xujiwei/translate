	package com.xjw.translate;

import java.lang.reflect.Field;

import org.unism.wang.widget.DatePicker;
import org.unism.wang.widget.TimePicker;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
	/**      
 * 项目名称：translate  
 * 实现功能：  添加备忘界面
 * 类名称：RemindAddActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年11月1日 上午11:14:25   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class RemindAddActivity extends Activity {
	
	private Context context;
	private Button btnBack;
	private DatePicker datePicker;
	private TimePicker timePicker;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remind_add);
		initView();
		setListener();
	}
	
	private void initView (){
		context = RemindAddActivity.this;
		btnBack = (Button)findViewById(R.id.btn_back_remind_add);
		timePicker = (TimePicker)findViewById(R.id.tp_remind_add);
		datePicker = (DatePicker)findViewById(R.id.dp_remind_add);
	}
	
	private void setListener(){
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		timePicker.setOnChangeListener(tp_onchanghelistener);
		datePicker.setOnChangeListener(dp_onchanghelistener);
	}
	
	
	//listeners
		DatePicker.OnChangeListener dp_onchanghelistener = new DatePicker.OnChangeListener() {
			@Override
			public void onChange(int year, int month, int day, int day_of_week) {
			}
		};
		TimePicker.OnChangeListener tp_onchanghelistener = new TimePicker.OnChangeListener() {
			@Override
			public void onChange(int hour, int minute) {
			}
		};
}
