	package com.xjw.translate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
	/**      
 * 项目名称：translate  
 * 实现功能：  备忘录列表界面
 * 类名称：RemindActivity   
 * 类描述：(该类的主要功能)
 * 创建人：徐纪伟 
 * E-mail: xujiwei558@126.com
 * 创建时间：2014年10月31日 下午10:10:00   
 * 修改人：   
 * 修改时间：   
 * 修改备注：   
 * @version    
 */
public class RemindActivity extends Activity {
	
	private Context context;
	private ListView listViewRemind;
	private LinearLayout layoutRemindAdd;
	private Button btnBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remind);
		initView();
		setListener();
	}
	
	/**
		* @方法名称: initView
		* @描述: 初始化视图
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年11月1日 上午11:04:21
	 */
	private void initView() {
		context = RemindActivity.this;
		listViewRemind = (ListView)findViewById(R.id.listview_remind);
		layoutRemindAdd = (LinearLayout)findViewById(R.id.layout_remind_add);
		btnBack = (Button)findViewById(R.id.btn_back_remind);
	}
	
	/**
		* @方法名称: setListener
		* @描述: 设置监听器
		* @param    
		* @return void 
		* @throws 
		* @author 徐纪伟
		* 2014年11月1日 上午11:09:15
	 */
	private void setListener(){
		//返回按钮
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		//添加备忘布局单击监听事件
		layoutRemindAdd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RemindActivity.this,RemindAddActivity.class);
				startActivity(intent);
				
			}
		});
		//listView单击事件
		listViewRemind.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}
		
		});
	}
}
